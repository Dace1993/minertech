<?php
/**
 * Product loop sale flash
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/sale-flash.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php /*if ( $product->is_on_sale() ) : ?>
	<?php $sale = get_post_meta( get_the_ID(), '_sale_price', true);
	$regular = get_post_meta( get_the_ID(), '_regular_price', true);
	$percent = ($regular - $sale)/$regular*100;
	$nombre_format_francais = number_format($percent, 0, ',', ' ');?>


	<i class="discount-percents d-flex justify-content-center align-items-center">
	<?php echo apply_filters( 'woocommerce_sale_flash', $nombre_format_francais.'<sup>%</sup>' , $post, $product ); ?>
	</i>
<?php endif;*/

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */?>
