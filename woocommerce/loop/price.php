<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>
<?php $regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
    if( $product->is_on_sale() ) {?>
        
    <div class="price has-old-price">

        <?php echo '€'.number_format($sale,2);?>
        <i class="old-price">
            <?php echo number_format($regular_price,2);?>
        </i>
    </div>

<?php }else{?>
    <?php if ( $price_html = $product->get_price_html() ) : ?>
        <div class="price"><?php echo '€'.number_format($regular_price,2);?></div>
    <?php endif; ?>


<?php };?>
