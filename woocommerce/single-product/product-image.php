<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . ( has_post_thumbnail() ? 'with-images' : 'without-images' ),
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
?>
<div class="col-lg-6"> 
   <div class="gallery">
	    <div id="carousel-gallery" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
				<div class="carousel-item active">
					<a data-fancybox="gallery" href="<?php the_post_thumbnail_url();?>" class="d-flex justify-content-center align-items-center"><?php echo get_the_post_thumbnail();?></a>
				</div>
				<?php do_action( 'woocommerce_product_thumbnails' );?>

				<a class="left carousel-control" href="#carousel-gallery" role="button" data-slide="prev">
	                <span class="glyphicon glyphicon-chevron-left"></span>
	            </a>
	            <a class="right carousel-control" href="#carousel-gallery" role="button" data-slide="next">
	                <span class="glyphicon glyphicon-chevron-right"></span>
	            </a>
			</div>
			<ol class="carousel-indicators flex-wrap">
				<li data-target="#carousel-gallery" data-slide-to="0" class="active d-flex justify-content-center align-items-center">
					<?php echo get_the_post_thumbnail();?>
				</li>
				<?php $attachment_ids = $product->get_gallery_image_ids();
				if ( $attachment_ids && has_post_thumbnail() ) {
					$count = 0;
					foreach ( $attachment_ids as $attachment_id ) {
						$count++;?>
						    <li data-target="#carousel-gallery" data-slide-to="<?php echo $count;?>" class="d-flex justify-content-center align-items-center">
								<img src="<?php echo $image_link = wp_get_attachment_url( $attachment_id );?>">
							</li>
					<?php }
				}?>
			</ol>
		</div>
	</div>
</div>


