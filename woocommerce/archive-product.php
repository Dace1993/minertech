
<?php
/**    Template Name: Shop page

 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;?>

<?php get_header();?>



<main id="main-content">  
    <div class="container">
		<?php /**
		 * Hook: woocommerce_before_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );

		?>
		<div class="products-list">
	        <div class="row">

				<?php
					/**
				 * Hook: woocommerce_archive_description.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action( 'woocommerce_archive_description' );
				

				if ( woocommerce_product_loop() ) {

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked wc_print_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
					$term = get_queried_object();
					if(isset($_GET['newest']) && !isset($_GET['sales'])){
						if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		                $args = array(
		                    'post_type' => 'product',
		                    'orderby' => 'post_date',
							'order' => 'DESC',
							'tax_query' => array($tax_query),
							'paged' => $paged,
		                );
	                	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;
	                }elseif(isset($_GET['sales'])){
	                	if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		                $args = array(
		                    'post_type' => 'product',

			                'meta_query'     => array(
						        array( 
						            'key'           => '_sale_price',
						            'value'         => 0,
						            'compare'       => '>',
						            'type'          => 'numeric'
						        )
						    ),
						    'tax_query' => array($tax_query),
						    'paged' => $paged,
					    );
					   	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;
				    }elseif(isset($_GET['sales']) && isset($_GET['newest'])){

				    	if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		                $args = array(
		                    'post_type' => 'product',

			                'meta_query'     => array(
						        array( 
						            'key'           => '_sale_price',
						            'value'         => 0,
						            'compare'       => '>',
						            'type'          => 'numeric',
						            'orderby' => 'post_date',
									'order' => 'DESC',
						        )
						    ),
						    'tax_query' => array($tax_query),
						    'paged' => $paged,
					    );
					   	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;

				    }elseif(isset($_GET['sales'])&&isset($_GET['top'])){
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

				    	if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
		                $args = array(
		                    'post_type' => 'product',
		                    'meta_key'  => 'wpb_post_views_count',
							'orderby'   => 'meta_value_num',
							'order' => 'DESC',
			                'meta_query'     => array(
						        array( 
						            'key'           => '_sale_price',
						            'value'         => 0,
						            'compare'       => '>',
						            'type'          => 'numeric'
						        )
						    ),
						    'tax_query' => array($tax_query),
						    'paged' => $paged,
					    );
					   	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;

				    }elseif(isset($_GET['top']) && !isset($_GET['sales'])){
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

				    	if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
		                $args = array(
		                	'post_type' => 'product',
		                	
							'meta_key'  => 'wpb_post_views_count',
							'orderby'   => 'meta_value_num',
		    				'tax_query' => array($tax_query),
		    				'paged' => $paged,
		    			);
					   	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;
				    }else{
				    	        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

				    	if(is_tax()){
				    		$tax_query = array('taxonomy' => 'product_cat', 'field' => 'slug' , 'terms' => $term->slug);
				    	};
				    	$args = array(
				    		'orderby' => 'post_date',
							'order' => 'ASC',
		                    'post_type' => 'product',
		                    'tax_query' => array($tax_query),
		                    'paged' => $paged,

		                );
	                	$my_query = new WP_Query( $args );
	                	$page_count =$my_query->max_num_pages;

				    } 				    		
wp_reset_query();

		         	if($my_query->have_posts()) {
						while ( $my_query->have_posts() ) {
							
							$my_query->the_post();
							/**
							 * Hook: woocommerce_shop_loop.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );

						
						}
							if($page_count >1 ){?>
								<?php simple_pagination($page_count, 1);?>
							<?php }

					}wp_reset_query();
					?>



		            <?php

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}

				/**
				 * Hook: woocommerce_after_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );

				/**
				 * Hook: woocommerce_sidebar.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				/**do_action( 'woocommerce_sidebar' );*/
				?>
			</div>
		</div>
	</div>
</main>

<?php get_footer( 'shop' );
