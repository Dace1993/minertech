<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();?>

<div class="container has-aside">
	<div class="my-cart">
    	<div class="row">
	        <?php 

            /**
             * My Account navigation.
             * @since 2.6.0
             */
            do_action( 'woocommerce_account_navigation' ); ?>
            <div class="col-lg-10">
            	<nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    	<?php $current = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    	$cart_link = get_permalink( wc_get_page_id( 'cart' ) );
                    	$address_link = get_permalink(get_option('woocommerce_myaccount_page_id')).'edit-address/';
                    	$order_link = get_permalink(get_option('woocommerce_myaccount_page_id')).'orders/';?>

                        <?php if($current == $cart_link){?>
                        	<li class="breadcrumb-item active" aria-current="page"><?php echo get_the_title( wc_get_page_id( 'cart' ) );?></li>
                        	<?php }else{?>
                        	<li class="breadcrumb-item"><a href="<?php echo $cart_link;?>"><?php echo get_the_title( wc_get_page_id( 'cart' ) );?></a></li>
                        <?php };?>
                        <?php $identic = strcmp($current,get_permalink(get_option('woocommerce_myaccount_page_id')).'edit-address/');
                        if($identic == 0){?>
                        	<li class="breadcrumb-item active" aria-current="page"><?php echo _e('Adrese','cetraszoles');?></li>
                        <?php }else{?>
                        	<li class="breadcrumb-item"><a href="<?php echo $address_link;?>"><?php echo _e('Adrese','cetraszoles');?></a></li>
                        <?php };?>
                        <?php $identical = strcmp($current,get_permalink(get_option('woocommerce_myaccount_page_id')).'orders/');
                        if($identical == 0){?>
                        	<li class="breadcrumb-item active" aria-current="page"><?php echo _e('Pasūtījums','cetraszoles');?></li>
                        <?php }else{?>
                        	<li class="breadcrumb-item"><a href="<?php echo $order_link;?>"><?php echo _e('Pasūtījums','cetraszoles');?></a></li>
                        <?php };?>
                    </ol>
                </nav>
                <div class="cart-table">

            		<?php
            		/**
            		 * My Account content.
            		 * @since 2.6.0
            		 */
            		do_action( 'woocommerce_account_content' );
            	?>
                </div>
            </div>
        </div>
    </div>
</div>