<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="container">
    <div class="login text-center">
        <div class="d-flex justify-content-center">
            <img src="<?php echo get_template_directory_uri(); ?>/app/assets/img/login-logo.png" alt="Četras Zoles">
        </div>
        <div class="form-wrapper">
            <div class="tabs">
        

			<?php endif; ?>
				<ul class="nav nav-tabs" id="myTab" role="tablist">
			        <li class="nav-item">
			            <a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab" aria-controls="home" aria-selected="true">IENĀKT</a>
			        </li>
					<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
				        <li class="nav-item">
				            <a class="nav-link" data-toggle="tab" href="#tab-2" role="tab" aria-controls="profile" aria-selected="false">REĢISTRĒTIES</a>
				        </li>
				    <?php endif;?>
			    </ul>
				<div class="tab-content">
		            <div class="tab-pane fade show active" id="tab-1" role="tabpanel">
		                            
					<form class="login-form" method="post">
						<h2 class="form-title"><?php echo _e('REĢISTRĒTIEM LIETOTĀJIEM','cetraszoles');?></h2>
						<?php do_action( 'woocommerce_login_form_start' ); ?>

                        <div class="form-group">
							<label for="username"><?php echo _e( 'E-pasts:', 'cetraszoles' ); ?>&nbsp;</label>
							<input type="text" class="form-control" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
						</div>
                        <div class="form-group">
							<label for="password"><?php echo _e( 'Parole', 'cetraszoles' ); ?>&nbsp;</label>
							<input class="form-control" type="password" name="password" id="password" autocomplete="current-password" />
						</div>

						<?php do_action( 'woocommerce_login_form' ); ?>

							<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

							<div class="submit form-group">

							<button type="submit" class="btn btn-primary" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php echo _e( 'Ienākt', 'cetraszoles' ); ?></button>
                            <div class="remind-password">

							<label>
								<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php echo _e( 'Atcerēties mani', 'cetraszoles' ); ?></span>
							</label>
							<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php echo _e( 'Aizmirsi paroli?', 'cetraszoles' ); ?></a>
							</div>
						</div>

						<?php do_action( 'woocommerce_login_form_end' ); ?>

					</form>
				</div>
	<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>


        <div class="tab-pane fade" id="tab-2" role="tabpanel">      


			<form method="post" class="login-form">
				<h2 class="form-title"><?php echo _e('REĢISTRĒTIES','cetraszoles');?></h2>

				<?php do_action( 'woocommerce_register_form_start' ); ?>

				<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                    <div class="form-group">
						<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
					</div>

				<?php endif; ?>

                <div class="form-group">
					<label for="reg_email"><?php echo _e( 'E-pasts', 'cetraszoles' ); ?>&nbsp;</label>
					<input type="email" class="form-control" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</div>

				<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                    <div class="form-group">
						<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
						<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
					</div>

				<?php endif; ?>

				<?php do_action( 'woocommerce_register_form' ); ?>

				<div class="submit form-group">
					<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
					<button type="submit" class="btn btn-primary" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php echo _e( 'Ienākt', 'cetraszoles' ); ?></button>
				</div>

				<?php do_action( 'woocommerce_register_form_end' ); ?>

			</form>

		</div>
		</div>
	</div>
	</div>
	</div>
	<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
