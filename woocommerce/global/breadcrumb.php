<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) {?>

	<div class="sort-by d-sm-flex align-items-center justify-content-between">
		<?php if(is_shop() || is_tax()){?>
			<ul class="links d-flex">
		        <li class="newest"><a href="<?php if(isset($_GET['sales'])=='true'||isset($_GET['top'])=='true'){ echo $_SERVER['REQUEST_URI'].'&newest=true';}else{echo '?newest=true';}?>" <?php if(isset($_GET['newest'])){ echo 'class="active"';};?>><?php echo _e('Newest','minertech');?></a></li>
		        <li class="sales"><a href="<?php if(isset($_GET['newest'])=='true'||isset($_GET['top'])=='true'){ echo $_SERVER['REQUEST_URI'].'&sales=true';}else{echo '?sales=true';}?>" <?php if(isset($_GET['sales'])){ echo 'class="active"';};?>><?php echo _e('Sale','minertech');?></a></li>
		        <li class="top"><a href="<?php if(isset($_GET['newest'])=='true'||isset($_GET['sales'])=='true'){ echo $_SERVER['REQUEST_URI'].'&top=true';}else{echo '?top=true';}?>" <?php if(isset($_GET['top'])){ echo 'class="active"';};?>><?php echo _e('Top','minertech');?></a></li>
		    </ul>
		<?php } else { ?>
		    <ol class="breadcrumb">
		        <li class="breadcrumb-item"><a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ));?>"><?php echo get_the_title( woocommerce_get_page_id( 'shop' ));?></a></li>
		        <li class="breadcrumb-item active" aria-current="page"><?php the_title();?></li>
		    </ol>
		<?php };?>

		<?php $terms = get_terms('product_cat');
		if($terms){?>
		    <ul class="links d-flex">
		    	<?php foreach($terms as $term){
		    		$term_id = $term->term_id?>
		    		<?php
		    		if(get_queried_object_id()){?>
		        		<li>
		        			<a <?php if($term_id == get_queried_object_id()){echo 'class="active"';};?> href="<?php echo get_term_link( $term );?>">
		        				<?php echo $term->name;?>
		        					
	        				</a>
	        			</li>
		       		<?php }else{?>
						<li>
		        			<a  href="<?php echo get_term_link( $term );?>">
		        				<?php echo $term->name;?>		        					
	        				</a>
	        			</li>
		       		<?php }
		       	};?>
		    </ul>
		<?php };?>
	</div>

<?php }
