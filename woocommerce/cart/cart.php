<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>


<div class="container">
    <div class="my-cart">
    	<nav aria-label="breadcrumb">
	        <ol class="breadcrumb">
	        	<?php $current = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	        	$cart_link = get_permalink( wc_get_page_id( 'cart' ) );
	        	$address_link = get_permalink(get_option('woocommerce_myaccount_page_id')).'edit-address/';
	        	$order_link = get_permalink(get_option('woocommerce_myaccount_page_id')).'orders/';?>

	            <?php if($current == $cart_link){?>
	            	<li class="breadcrumb-item active" aria-current="page"><?php echo get_the_title( wc_get_page_id( 'cart' ) );?></li>
	            	<?php }else{?>
	            	<li class="breadcrumb-item"><a href="<?php echo $cart_link;?>"><?php echo get_the_title( wc_get_page_id( 'cart' ) );?></a></li>
	            <?php };?>
	            <?php $identic = strcmp($current,get_permalink(get_option('woocommerce_myaccount_page_id')).'edit-address/');
	            if($identic == 0){?>
	            	<li class="breadcrumb-item active" aria-current="page"><?php echo _e('Adrese','cetraszoles');?></li>
	            <?php }else{?>
	            	<li class="breadcrumb-item"><a href="<?php echo $address_link;?>"><?php echo _e('Adrese','cetraszoles');?></a></li>
	            <?php };?>
	            <?php $identical = strcmp($current,get_permalink(get_option('woocommerce_myaccount_page_id')).'orders/');
	            if($identical == 0){?>
	            	<li class="breadcrumb-item active" aria-current="page"><?php echo _e('Pasūtījums','cetraszoles');?></li>
	            <?php }else{?>
	            	<li class="breadcrumb-item"><a href="<?php echo $order_link;?>"><?php echo _e('Pasūtījums','cetraszoles');?></a></li>
	            <?php };?>
	        </ol>
	    </nav>
	    <div class="row">
			<div class="col-lg-8">

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

	<?php do_action( 'woocommerce_before_cart_table' ); ?>
	 <div class="cart-table">
	<h2 class="block-title"><?php the_title();?></h2>
	<table class="table" cellspacing="0">
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						
						<td class="thumbnail" data-title="<?php esc_attr_e( 'Image', 'woocommerce' ); ?>">
						<?php
						$thumbnail = get_the_post_thumbnail($product_id,'woocommerce_thumb');;

						if ( ! $product_permalink ) {
							echo wp_kses_post( $thumbnail );
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
						}
						?>
						</td>
						
						<td class="details" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<?php
						if ( ! $product_permalink ) {
							echo '<h3 class="name">'.wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ).'</h3>';
						} else {
							echo '<h3 class="name">'.wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) ).'</h3>';
						}

						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>' ) );
						}
						?>
						</td>

					
						<td class="quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'   => "cart[{$cart_item_key}][qty]",
								'input_value'  => $cart_item['quantity'],
								'max_value'    => $_product->get_max_purchase_quantity(),
								'min_value'    => '0',
								'product_name' => $_product->get_name(),
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>
						</td>

						<td class="price" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
							<?php
								echo '<strong>'.apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ).'</strong>'; // PHPCS: XSS ok.
							?>
						</td>
						<td class="product-remove">
							<?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"></a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>

					</tr>
					<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>

			
					<?php if ( wc_coupons_enabled() ) { ?>
					<tr>
				<td colspan="6" class="actions">

						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>


					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</td>
			</tr>
					<?php } ?>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>
</div>
</div>

<div class="col-lg-4">
	<div class="checkout-details">

	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>
</div>
</div>
</div>
</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
