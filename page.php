<?php get_header(); ?>
<?php if(have_posts()) { ?>
    <?php while ( have_posts()) { the_post(); ?>
        <main id="main-content">  
            <div class="container">
                <article class="post hentry">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title();?></h1>
                    </header>

                    <div class="entry-content">
                        <div class="row consulting d-flex justify-content-between">
                            <?php if(get_the_content()){?>
                                <div class="col-lg-7">
                                    <?php the_content();?>
                                </div>
                            <?php };?>
                            <div class="col-image col-lg-4 d-flex align-items-center justify-content-end">
                                <?php the_post_thumbnail('content_logo');?>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </main>
    <?php };
};?>
<?php get_footer(); ?>