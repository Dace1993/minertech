<?php get_header(); ?>
    <div class="container">
        <div class="page-header">
            <h1 class="page-title"><?php single_cat_title(); ?></h1>
        </div>

        <main id="main-content">
            <div class="posts-list">
                <div class="categories">
                    
       
                </div>

                <div class="row d-flex justify-content-between flex-wrap">                 
                        <div class="col-lg-6 col-md-6">
                            <article class="post">
                                <header class="entry-header">
                                    <h2 class="entry-title d-flex align-items-end">
                                        <a href="<?php the_permalink(); ?>"  class="d-flex align-items-end justify-content-center">
                                            <span><?php the_title(); ?></span>
                                        </a>
                                    </h2>
                                </header>

                                <?php the_post_thumbnail(); ?>
                            </article>                                
                        </div>
                
                </div>
            </div>

            <nav class="text-center">
               <?php simple_pagination(); ?>
            </nav>
        </main>
    </div>
<?php get_footer(); ?>