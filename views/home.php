<?php 
/*  
    Template Name: Home
*/
?>

<?php get_header(); ?>
<main id="main-content">
    <?php $hero_slider = get_field('hero_slider');
    if($hero_slider){?>
        <section class="hero-slider">
            <div class="container">
                <div class="owl-carousel">
                    <?php foreach($hero_slider as $slider){?>
                        <div>
                            <div class="container d-flex align-items-center">
                                <div class="text">
                                    <h1><?php echo $slider['slider_text'];?></h1>
                                </div>
                            </div>
                        </div>
                    <?php };?>
                </div>
            </div>

            <i class="scroll-action"></i>
        </section>
    <?php };?>
    <?php $article_section = get_field('article_section');
    if($article_section){?>
        <section class="articles section">
            <div class="container">
                <div class="row">
                    <?php foreach($article_section as $article){?>
                        <div class="col-lg-4 col-md-4">
                            <article class="post-article d-flex flex-column">
                                <div class="thumbnail">
                                    <a href="<?php if($article['article_link']){echo $article['article_link']['url'];};?>">
                                        <?php $article_thumb = $article['article_thumbnail'];
                                        $image_sizes = 'home_service_thumb';
                                        $image_urls = $article_thumb['sizes'][$image_sizes];?>
                                        <img src="<?php echo $image_urls; ?>" alt="">
                                    </a>
                                </div>

                                <div class="details">
                                    <h2 class="name">
                                        <a href="<?php if($article['article_link']){ echo $article['article_link']['url'];};?>"><?php if($article['article_link']){echo $article['article_link']['title'];};?></a>
                                    </h2>

                                    <p><?php echo $article['article_text'];?></p>
                                </div>
                            </article>
                        </div>
                    <?php };?>
                </div>
            </div>
        </section>
    <?php };?>
    <?php wp_reset_query();
    $shop_section = get_field('shop_section');
    if($shop_section){?>
        <section class="shop section">
            <div class="container">
                <h2 class="section-title text-center"><?php echo $shop_section['section_title'];?></h2>

                <div class="shop-tabs">
                    <ul class="nav nav-tabs justify-content-center"  role="tablist">
                        <?php $shop_block = $shop_section['shop_block'];
                        $counts = 0;
                        foreach($shop_block as $shop){
                            $counts++;?>
                            <li class="nav-item">
                                <a class="nav-link <?php if($counts==1){echo 'active';};?>" data-toggle="tab" href="#tab<?php echo $counts;?>" role="tab" aria-selected="true"><?php echo $shop['tab_title'];?></a>
                            </li>
                        <?php };?>
                    </ul>

                    <div class="tab-content">
                        <?php $shop_block = $shop_section['shop_block'];
                        $counting = 0;
                        foreach($shop_block as $shop){
                            $counting++;?>
                            <div class="tab-pane fade <?php if($counting==1){echo 'show active';};?>" id="tab<?php echo $counting;?>" role="tabpanel">
                                <div class="products-carousel">
                                    <div class="owl-carousel">
                                        <?php $product_item = $shop['product_item'];
                                        if($product_item){
                                            foreach($product_item as $post){
                                                setup_postdata($post); ?>
                                                    <?php wc_get_template_part( 'content', 'product' );?>

                                            <?php };
                                        };wp_reset_query()?>
                                    </div>
                                </div>
                            </div>
                        <?php };?>
                    </div>
                </div>
    
                <div class="see-all text-center">
                    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) );?>" class="btn btn-primary black"><?php echo __('See all','minertech');?></a>
                </div>
            </div>
        </section>
    <?php };?>
    <?php $pricing_block = get_field('pricing_block');
    if($pricing_block){?>
        <section class="pricing section">
           <div class="container">
                <div class="title-holder text-center">
                    <h2 class="section-title"><?php echo $pricing_block['block_title'];?></h2>
                    <p><?php echo $pricing_block['block_text'];?></p>
                </div>
                <?php $pricing_list = $pricing_block['home_pricing_list'];
                if($pricing_list){?>
                    <div class="pricing-list">
                        <div class="row">
                            <?php $x = 0; foreach($pricing_list as $pricing){
                                $x ++;
                                if($pricing['price_list']){?>
                                    <div class="col-lg-4">
                                        <div class="price-block">
                                            <h3 class="plan-name"><span><?php echo $pricing['plan_name'];?></span></h3>
                                            
                                            
                                            <?php $s= 0;
                                            foreach($pricing['price_list'] as $price){
                                                $s++;?>
                                                <?php if (strpos($price['price'], '€') !== 0) {
                                                    if($s==1){?>
                                                        <div class="contract-basis">
                                                            <table class="table">
                                                                <tbody>
                                                    <?php };?>
                                                <?php }else{
                                                     if($s==1){?>
                                                        <div class="devices-amount">
                                                            <span class="title"><?php echo _e('HOW MANY DEVICES','minertech');?></span>
                                                            <form>
                                                                <div class="radio-list d-flex">
                                                    <?php };?>
                                                <?php }
                                            }?>
                                                
                                                
                                            <?php $count = 0;
                                            foreach($pricing['price_list'] as $price){
                                                $count++;?>
                                                <?php if (strpos($price['price'], '€') !== 0) {?>
                                                    <tr>
                                                        <td><?php echo $price['devices_amount'];?></td>
                                                        <td>
                                                            <span <?php if (strpos($price['price'], '€') !== false) {echo 'class="price"';};?>><?php echo $price['price'];?></span>
                                                        </td>
                                                    </tr>
                                                <?php }else{?>
                                                    <input type="radio" value="<?php echo $price['price'];?>" name="devices-amount" id="rb-<?php echo $x;?>-<?php echo $count;?>" <?php if($count == 1){echo 'checked=""';};?>>
                                                    <label for="rb-<?php echo $x;?>-<?php echo $count;?>"><?php echo $price['devices_amount'];?></label>
                                                <?php };?>
                                            <?php };?> 
                                                   
                                            <?php $i = 0;foreach($pricing['price_list'] as $price){ $i++;
                                                if (strpos($price['price'], '€') !== 0){
                                                    if($i==1){?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    <?php }
                                                }else{
                                                    if($i==1){?>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        <span class="price"><?php echo $price['price'];?></span>
    
                                                    <?php };
                                                }
                                            }?>
                                        </div>
                                    </div>
                                <?php };
                            };?>
                        </div>
                    </div>
                <?php };
                $home_electricity = $pricing_block['home_electricity_prices_group'];
                if($home_electricity){?>
                    <div class="electricity-price">
                        <div class="row">
                            <?php if($home_electricity['block_title']){?>
                                <div class="col-lg d-lg-flex align-items-center justify-content-center">
                                   <h4 class="b-name"><?php echo $home_electricity['block_title'];?></h4>
                                </div>
                            <?php };?>
                            <?php $electricity_price = $home_electricity['electricity_price_list'];
                            if($electricity_price){
                                foreach($electricity_price as $electricity){?>
                                    <div class="col-lg d-xl-flex align-items-center justify-content-center">
                                        <div><?php echo $electricity['supply_method'];?> <strong><?php echo $electricity['prices'];?></strong> <?php echo _e('kWh','minertech');?></div>
                                    </div>
                                <?php };
                            };?>
                        </div>
                    </div>
                <?php };?>
                <?php $args = array(
                  'post_type' => 'page',
                  'meta_key' => '_wp_page_template',
                  'meta_value' => 'views/hosting.php'
                );
                $my_query = new WP_Query($args)?>
                <?php if($my_query -> have_posts()) : ?>
                    <?php while ( $my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="learn-more text-center">
                            <a href="<?php the_permalink();?>" class="btn btn-primary"><span><?php echo _e('Learn more','minertech');?></span></a>
                        </div>
                    <?php endwhile;
                endif;?>
           </div> 
        </section>
    <?php };wp_reset_query();?>
    <?php $contact_form_group = get_field('contact_form_group');
    if($contact_form_group){?>
        <section class="contact-form section">
            <div class="container">
                <h2 class="section-title text-center"><?php echo $contact_form_group['title']?></h2>
                <div class="row">
                    <?php echo do_shortcode($contact_form_group['contact_form']);?>

                    <div class="submit col-lg-12 text-center">
                        <button class="btn btn-primary black"><?php echo _e('Send','minertech');?></button>
                    </div>

                </div>
            </div>
        </section>
    <?php };?>
</main>
<?php get_footer(); ?>

