<?php 
/*  
    Template Name: Consulting
*/
?>
<?php get_header(); ?>
<?php if(have_posts()) { ?>
    <?php while ( have_posts()) { the_post(); ?>
        <main id="main-content">  
            <div class="container">
                <article class="post hentry">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title();?></h1>
                    </header>

                    <div class="entry-content">
                        <div class="row consulting d-flex justify-content-between">
                            <?php if(get_the_content()){?>
                                <div class="col-lg-7">
                                    <?php the_content();?>
                                </div>
                            <?php };?>
                            <div class="col-image col-lg-4 d-flex align-items-center justify-content-end">
                                <?php the_post_thumbnail('content_logo');?>
                            </div>
                        </div>
                    </div>
                </article>
                <?php $price_block = get_field('price_block');
                if($price_block && $price_block['price_list']){?>
                    <section class="price-list section">
                        <div class="container"> 
                            <h2 class="section-title text-center"><?php echo $price_block['block_title'];?></h2>             
                            <div class="vertical-info-list text-center">
                                <ul>
                                    <?php foreach($price_block['price_list'] as $price_list){?>
                                        <li><span><?php echo $price_list['service'];?></span> <i class="price"><?php echo $price_list['price'];?></i></li>
                                    <?php };?>
                                </ul>
                            </div>
                        </div>
                    </section>
                <?php };?>
            </div>
        </main>
    <?php };
};?>
<?php get_footer(); ?>