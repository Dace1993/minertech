<?php 
/*  
    Template Name: Hosting
*/
?>

<?php get_header(); ?>
<?php if(have_posts()) { ?>
    <?php while ( have_posts()) { the_post(); ?>
        <main id="main-content">           
            <section class="pricing inner section">
               <div class="container">
                    <div class="title-holder text-center">
                        <h2 class="section-title"><?php the_title();?></h2>
                        <?php the_content();?>
                    </div>
                    
                    <?php $pricing_list = get_field('pricing_list');
                    if($pricing_list){?>
                        <div class="pricing-list">
                            <div class="row">
                                <?php foreach($pricing_list as $pricing){
                                    if($pricing['price_list']){?>
                                        <div class="col-lg-4">
                                            <div class="price-block d-flex flex-column justify-content-between">
                                                <h3 class="plan-name"><span><?php echo $pricing['plan_name'];?></span></h3>
                                                <div class="contract-basis">
                                                    <table class="table per-device">
                                                        <tbody>
                                                            <?php foreach($pricing['price_list'] as $price){?>
                                                                <tr>
                                                                    <td><?php echo $price['devices_amount'];?></td>
                                                                    <td>
                                                                        <span <?php if (strpos($price['price'], '€') !== false) {echo 'class="price"';};?>><?php echo $price['price'];?></span>
                                                                    </td>
                                                                </tr>
                                                            <?php };?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <?php };
                                };?>
                            </div>
                        </div>
                    <?php };?>
                    <?php $electricity_prices_group = get_field('electricity_prices_group');
                    if($electricity_prices_group && $electricity_prices_group['electricity_price_list']){?>
                        <div class="electricity-price">
                            <div class="row">
                                <div class="col-lg d-lg-flex align-items-center justify-content-center">
                                   <h4 class="b-name"><?php echo $electricity_prices_group['block_title'];?></h4>
                                </div>
                                <?php foreach($electricity_prices_group['electricity_price_list'] as $electricity_prices){?>
                                    <div class="col-lg d-xl-flex align-items-center justify-content-center">
                                        <div><?php echo $electricity_prices['supply_method'];?> <?php if($electricity_prices['prices']){ echo '<strong>'.$electricity_prices['prices'].'</strong>';?> <?php echo _e('kWh','minertech');};?></div>
                                    </div>
                                <?php };?>
                            </div>
                        </div>
                    <?php };?>
               </div> 
            </section>
            <?php $included_services = get_field('included-services');
            if($included_services && $included_services['services']){?>
                <section class="included-services section">
                    <div class="container">
                        <h2 class="section-title text-center"><?php echo $included_services['block_title'];?></h2>
                        <div class="vertical-info-list">
                            <ul class="text-center">
                                <?php foreach($included_services['services'] as $service){?>
                                    <li>
                                        <span><?php echo $service['service_name'];?></span>
                                    </li>
                                <?php };?>
                            </ul>
                        </div>
                    </div>
                </section>
            <?php };?>
        </main>
    <?php };
};?>
<?php get_footer(); ?>
