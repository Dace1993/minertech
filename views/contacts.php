<?php 
/*
Template Name: Contacts
*/ 
get_header(); ?>
<?php if(have_posts()) { ?>
    <?php while ( have_posts()) { the_post(); ?>
        <main id="main-content">  
            <div class="contacts">
                <section class="contacts-details section">
                    <div class="container">
                        <h2 class="section-title text-center"><?php the_title();?></h2>
                        <?php $contacts_details_group = get_field('contacts_details_group');
                        if($contacts_details_group){?>
                            <div class="details">
                                <div class="row">
                                    <?php $details_information = $contacts_details_group['details_information'];
                                    foreach($details_information as $details){?>
                                        <div class="col-lg-4 d-flex align-items-center justify-content-center">
                                            <p><?php echo $details['contact'];?></p> 
                                        </div>
                                    <?php };?>
                                    <?php if($contacts_details_group['phone']){?>
                                        <div class="col-lg-6 d-flex align-items-center justify-content-center">
                                            <span class="phone"><?php echo $contacts_details_group['phone'];?></span>
                                        </div>
                                    <?php };?>
                                    <?php if($contacts_details_group['email']){?>
                                        <div class="col-lg-6 d-flex align-items-center justify-content-center">
                                            <span class="email"><?php echo $contacts_details_group['email'];?></span>
                                        </div>
                                    <?php };?>
                                </div>
                            </div>
                        <?php };?>
                    </div>
                </section>

                <section class="form section">
                    <div class="container">
                        <?php the_content();?>
                        <div class="submit col-lg-12 text-center">
                            <button type="submit" class="btn btn-primary"><span><?php echo _e('Send','minertech');?></span></button>
                        </div>
                    </div>
                </section>
            </div>
        </main>
    <?php };
};?>
<?php get_footer();?>
