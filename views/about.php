<?php 
/*  
    Template Name: About
*/
?>
<?php get_header(); ?>
<?php if(have_posts()) { ?>
    <?php while ( have_posts()) { the_post(); ?>
        <main id="main-content">  
            <div class="container">
                <article class="post hentry">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title();?></h1>
                    </header>

                    <div class="entry-content">
                        <div class="row d-flex justify-content-between">
                            <?php if(get_the_content()){?>
                                <div class="col-lg-7">
                                    <?php the_content();?>
                                </div>
                            <?php };?>
                            <div class="col-lg-4 d-lgflex align-items-center justify-content-end">
                                <?php the_post_thumbnail('content_logo');?>
                            </div>
                        </div>
                        <?php $gallery_list = get_field('gallery_list');
                        if($gallery_list){?>
                            <div class="gallery-list">
                                <div class="row">
                                    <?php foreach($gallery_list as $gallery){?>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <a data-fancybox="gallery" class="gallery-item" href="<?php echo $gallery['image']['url']; ?>">
                                                <?php $gallery_thumb = $gallery['image'];
                                                $image_sizes = 'gallery_thumb';
                                                $image_urls = $gallery_thumb['sizes'][$image_sizes];?>
                                                <img src="<?php echo $image_urls; ?>" alt="">
                                            </a>
                                        </div>
                                    <?php };?>
                                </div>
                            </div>
                        <?php };?>
                    </div>
                </article>
            </div>
        </main>
    <?php };
};?>
<?php get_footer(); ?>
