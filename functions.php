<?php

/**
 * SW engine. 
 * Global Template functions.
 */

require get_template_directory() . '/inc/base.php';

/**
 * Structure.
 * Template functions used throughout the theme.
 */

require get_template_directory() . '/inc/theme.php';

/**
 * Widgets.
 * Template widgets used throughout the theme.
 */

require get_template_directory() . '/inc/widgets.php';


function simple_pagination($pages = '', $range = 2)
{  
   $showitems = ($range * 2)+1;  

   global $paged;
    if(empty($paged)) $paged = 1;

   if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }  

   if(1 != $pages)
    {?>


<ul class=" col-lg-12 pagination justify-content-center align-items-center flex-wrap">
    <?php 
    for ($i=1; $i <= $pages; $i++){
        if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
            echo ($paged == $i)? '<li class="page-item "><a class="btn btn-primary" href="'.get_pagenum_link($i).'">'.$i.'</a></li>':'<li class="page-item"><a class="btn btn-primary black" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
        }
    }?>

</ul>
   <?php     
 
    }
}


    remove_action( 'woocommerce_single_product_summary',
        'woocommerce_template_single_meta', 40 );
    remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail', 10);
    remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering' , 30);

    remove_action( 'woocommerce_single_product_summary',
        'woocommerce_template_single_add_to_cart', 30 );
     
 
    

    


add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = -1;
  return $cols;
}
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


