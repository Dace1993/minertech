<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.theme.default.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
        <link href="<?php echo get_template_directory_uri(); ?>/app/assets/css/style.css" rel="stylesheet" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
    </head>

    <body <?php body_class(); ?>>
        <header id="main-header">
            <nav class="navbar">
                <div class="container-fluid align-self-start">
                    <div class="row">
                        <?php $defaults = array(
                          'menu'            => 'Top menu left',
                          'container'       => '',
                          'container_class' => '',
                          'container_id'    => '',
                          'menu_class'      => 'menu w-col w-col-8 nav-column',
                          'menu_id'         => '',
                          'echo'            => false,
                          'fallback_cb'     => 'wp_page_menu',
                          'before'          => '',
                          'after'           => '',
                          'link_before'     => '',
                          'link_after'      => '',
                          'items_wrap'      => '%3$s',
                          'depth'           => 0,
                          'walker'          => ''
                        );

                        $find = array('><a'   ,   '<li', 'menu-item ');
                        $replace = array('><a class="nav-link d-flex align-items-center justify-content-center"'  ,  '<li', 'col d-md-flex d-none justify-content-center ');

                        echo  str_replace( $find, $replace,   wp_nav_menu( $defaults )  );
                        //wp_nav_menu( $defaults );?>
                        <?php if(get_field('header_logo','options')){?>
                            <div class="col-md d-flex justify-content-sm-center">
                                <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                                    <?php $header_thumb = get_field('header_logo','options');
                                    $image_sizes = 'header_logo_thumb';
                                    $image_urls = $header_thumb['sizes'][$image_sizes];?>
                                    <img src="<?php echo $image_urls; ?>" alt="">
                                </a>
                            </div>
                        <?php };?>
                        <?php $defaults1 = array(
                          'menu'            => 'Top menu right',
                          'container'       => '',
                          'container_class' => '',
                          'container_id'    => '',
                          'menu_class'      => 'menu w-col w-col-8 nav-column',
                          'menu_id'         => '',
                          'echo'            => false,
                          'fallback_cb'     => 'wp_page_menu',
                          'before'          => '',
                          'after'           => '',
                          'link_before'     => '',
                          'link_after'      => '',
                          'items_wrap'      => '%3$s',
                          'depth'           => 0,
                          'walker'          => ''
                        );

                        $find1 = array('><a'   ,   '<li', 'menu-item ');
                        $replace1 = array('><a class="nav-link d-flex align-items-center justify-content-center"'  ,  '<li', 'col d-md-flex d-none justify-content-center ');

                        echo  str_replace( $find1, $replace1,   wp_nav_menu( $defaults1 )  );
                        //wp_nav_menu( $defaults );?>
                    </div>
                </div>

                <!-- Mobile menu -->

                <div class="mobile-nav-wrapper">
                    <ul class="navbar-nav nav">
                        <?php  wp_nav_menu( array(
                            'menu'              => 'Top menu left',
                            'depth'             => 3,
                            'container'         => true,
                            'container_class'   => 'nav-item',
                            'container_id'      => '',
                            'menu_class'        => 'navbar-nav nav d-xl-flex flex-row',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())

                        ); ?>
                        <?php  wp_nav_menu( array(
                            'menu'              => 'Top menu right',
                            'depth'             => 3,
                            'container'         => true,
                            'container_class'   => 'nav-item btn-li',
                            'container_id'      => '',
                            'menu_class'        => 'navbar-nav nav d-xl-flex flex-row',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())

                        ); ?>
                    </ul>

                    <a href="#" class="close-menu"></a>
                </div>

                <a href="#" class="menu-toggle"><i class="icon"></i></a>

                <!-- /Mobile menu -->
            </nav>

            <div class="lang-bar text-center">
                <?php dynamic_sidebar('sidebar'); ?> 
            </div>
            <?php wp_head(); ?>
        </header>
        <!-- mfunc setPostViews(get_the_ID()); --><!-- /mfunc -->