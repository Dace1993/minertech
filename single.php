<?php get_header(); ?>
<?php if(have_posts()) : ?>
    <?php while ( have_posts()) : the_post(); ?>
        <main id="main-content">
            <div class="container">
                <div class="post-view">
                    <article class="post">
                        <header class="entry-header">
                            <h1 class="entry-title"><?php the_title();?></h1>
                        </header>                        
                        <div class="entry-content">
                              <?php the_content();?>                            
                        </div>                        
                    </article>
                </div>
            </div>
        </main>
    <?php endwhile;?>
<?php endif;?>
<?php get_footer();?>
