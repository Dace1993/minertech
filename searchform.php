<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    
        <input type="text" class="form-control" placeholder="<?php echo _e('Meklēt','vef');?>" value="<?php echo get_search_query(); ?>" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( '', 'submit button' ); ?>" />
 
</form>