<?php 

get_header(); ?>
<main id="main-content">  
    <div class="container">
        <div class="products-list">
            <div class="row">        
                <?php wp_reset_query();
                $args = array(
                    'post_type' => 'post',
                    'orderby' => 'date',
                    'post_per_page' => -1,
                );
                $my_query = new WP_Query( $args );

                if($my_query -> have_posts()) : ?> 
                    <?php while (  $my_query -> have_posts()) :  $my_query -> the_post();?>
                        <div class="<?php if(is_shop()||is_tax()){echo 'col-xl-3 col-lg-4 col-md-6';};?>">
                            <div class="product">
                                <div class="thumbnail">
                                    <a class="d-flex align-items-center justify-content-center" href="<?php echo get_the_permalink();?>">
                                        <?php the_post_thumbnail('shop_thumb', ['class' => 'img-responsive responsive--full', 'title' => 'Feature image']);?>
                                        
                                    </a>
                                </div>  
                                <div class="details">
                                    <h3 class="name">
                                        <a href="<?php echo get_the_permalink();?>"><?php the_title();?></a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;
                endif;?>                
            </div>
        </div>
    </div>
</main>

<?php get_footer();?>