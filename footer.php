        <footer id="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 d-md-flex align-items-center">
                        <div class="footer-holder d-md-flex align-items-center">
                            <?php if(get_field('footer_logo','options')){?>
                                <div class="footer-logo">
                                    <?php $footer_thumb = get_field('footer_logo','options');
                                    $image_sizes = 'footer_logo_thumb';
                                    $image_urls = $footer_thumb['sizes'][$image_sizes];?>
                                    <img src="<?php echo $image_urls; ?>" alt="">
                                </div>
                            <?php };?>
                            <?php $social_media = get_field('social_media','options');
                            if($social_media){?>
                                <ul class="social-media d-flex justify-content-center">
                                    <?php foreach($social_media as $media){?>
                                        <li>
                                            <a target="_blank" href="<?php echo $media['link'];?>">
                                                <img src="<?php echo $media['image'];?>">
                                            </a>
                                        </li>
                                    <?php };?>
                                </ul>
                            <?php };?>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 d-md-flex justify-content-end align-items-center">
                        <?php  wp_nav_menu( array(
                            'menu'              => 'Footer menu',
                            'depth'             => 3,
                            'container'         => true,
                            'container_class'   => 'nav-item btn-li',
                            'container_id'      => '',
                            'menu_class'        => 'navbar-nav nav d-xl-flex flex-row',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())

                        ); ?>
                    </div>
                </div>
            </div>
        </footer>
        
        <div id="nav-overlay"></div>

        <!-- JS files -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/js/functions.js"></script>

        <!--IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo get_template_directory_uri(); ?>/app/assets/js/ie10-viewport-bug-workaround.js"></script>
        <?php wp_footer();?>
    </body>
</html>