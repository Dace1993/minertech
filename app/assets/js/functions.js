$(function(){
	$('.submit.col-lg-12 button').click(function(){
		$('.nf-field-element .btn-primary').click();
	})
	$('body').mouseover(function(){
		var x = 0;
		$('.pricing-list .col-lg-4').each(function(){
			x += 1;
			var i = 0;
			$(this).find('.radio-list input').each(function(){
				i += 1;
				
				if($(this).is(':checked')) {
					var checked = $(this).val();
					console.log(checked);
					console.log(x);
					$('.pricing-list .col-lg-4:nth-child('+x+') span.price').text(checked);
				}
			})
		})
	})
	var clean_uri = window.location.href;
	if($(".newest .active")[0]){
		$('.newest .active').attr('href',clean_uri.replace('newest=true',''));
		$(".newest .active").click(function(){
			setTimeout(function() {
				window.location.replace(clean_uri.replace('newest=true',''));
			}, 1000)
		})
		$('.top a').attr('href',clean_uri.replace('newest=true','top=true'));
	}

	if($(".sales .active")[0]){
		$('.sales .active').attr('href',clean_uri.replace('sales=true',''));
		$(".sales .active").click(function(){
			setTimeout(function() {
				window.location.replace(clean_uri.replace('sales=true',''));
			}, 1000)
		})
	}
	if($(".top .active")[0]){
		$('.top .active').attr('href',clean_uri.replace('top=true',''));
		$(".top .active").click(function(){
			setTimeout(function() {
				window.location.replace(clean_uri.replace('top=true',''));
			}, 1000)
		})
		$('.newest a').attr('href',clean_uri.replace('top=true','newest=true'));
	}

	$('.pricing-list .col-lg-4').each(function(){
		var euro = $(this).find("td .price:contains(€)");
		if(euro.length == 0){
			var table = $(this).children();
			table.children().each(function(){
				var parent = $(this).attr('class');
				if(parent == 'contract-basis'){
					$(this).children().removeClass('per-device');
				}
			})
		}
	})
	/* Call the OWL */

	if($('.hero-slider .owl-carousel').length) {
		$('.hero-slider .owl-carousel').owlCarousel({
			margin: 0,
			items: 1,
			autoplay: true,
			autoHeight:true,
			loop: true,
    		nav: true,
    		dots: false,
		});
	}

	if($('.shop .products-carousel').length) {
		$('.shop .products-carousel .owl-carousel').owlCarousel({
			margin: 45,
			items: 4,
			loop: true,
    		nav: true,
    		dots: false,
    		responsive:{
    			0:{
		            items:1
		        },

		        576:{
		            items:2
		        },

		        992:{
		            items:3
		        },

		        1200:{
		        	items:4
		        }
		    }
		});
	}
	

	/* RWD Menu */

 	$('#main-header .menu-toggle').on('click', function() {
 		$(this).toggleClass('opened');
 		
 		if($(this).hasClass('opened')) {
			$('#nav-overlay').addClass('active');
 			$('#main-header .mobile-nav-wrapper').animate({ right: 0 }, 250);

		} else {
			$('#nav-overlay').removeClass('active');
 			$('#main-header .mobile-nav-wrapper').animate({ right:'-100%' }, 150);
		}
 				
 		return false;
 	});

 	$(document).on('click', '#main-header .close-menu, #nav-overlay', function(){
 		$('#main-header .menu-toggle').removeClass('opened');

		$('#nav-overlay').removeClass('active');
		$('#main-header .mobile-nav-wrapper').animate({right:'-100%' }, 150);	

		return false;
	});

});